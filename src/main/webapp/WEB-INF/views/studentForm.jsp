<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>student form</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
</head>
<body>

<div class="container">
     
     <h3> STUDENT FORM </h3>
        <form:form method="POST" modelAttribute="student" enctype="multipart/form-data">
	<table>
   	    		<tr>
					<td><label>Select a Photo to Upload: <form:input path="file" type="file" class="form-control"/></label></td>
					<td><img id="image" alt="" src="${pageContext.request.contextPath}/download?file=${student.imageName}" width="150" height="100" border="1px"/></td>
				</tr>
				
				<tr>
					<td><form:label path="firstName"> First Name </form:label></td>
					<td><form:input path="firstName" class="form-control"/></td>
				</tr>
				
				<tr>
					<td><form:label path="lastName"> Last Name </form:label></td>
					<td><form:input path="lastName" class="form-control"/></td>
				</tr>
				
				<tr>
					<td><form:label path="subject"> Subject</form:label></td>
					<td><form:input path="subject" class="form-control"/></td>
				</tr>
				
				<tr>
					<td><form:label path="address">Address</form:label></td>
					<td><form:input path="address" class="form-control"/></td>
				</tr>
				
				<tr>
					<td><form:label path="email">Email</form:label></td>
					<td><form:input path="email" class="form-control"/></td>
				</tr>
			
				<tr>
					<td><form:label path="contactno">Contact no</form:label></td>
					<td><form:input path="contactno" class="form-control"/></td>
				</tr>
				
				<tr>
					<td><form:label path="joineddate">Joined date</form:label></td>
					<td><form:input path="joineddate" type="date" class="form-control"/></td>
				</tr>
				
				<tr>
					<td><form:label path="coursestartdate">Course start date</form:label></td>
					<td><form:input path="coursestartdate" type="date" class="form-control"/></td>
				</tr>

				<tr>
					<td><form:label path="courseenddate">Course end date</form:label></td>
					<td><form:input path="courseenddate" type="date" class="form-control"/></td>
				</tr>
				
				<tr>
					<td><form:label path="fee">Fee</form:label></td>
					<td><form:input path="fee" type="number" step="0.01" class="form-control"/></td>
				</tr>
				
				<tr>
				<td></td>
       			<td><input type="submit" class="btn-btn-primary" value="save"></td>
     			 </tr>
     			 
			</table>
		
			<form:hidden path="id"/>
			<form:hidden path="imageName"/>
		</form:form>
			
			<script type="text/javascript">
              document.getElementById("file").onchange = function () {
    
	          var reader = new FileReader();
              reader.onload = function (e) {
                  // get loaded data and render thumbnail.
               document.getElementById("image").src = e.target.result;
            };

             // read the image file as a data URL.
                 reader.readAsDataURL(this.files[0]);
              };
          </script>
