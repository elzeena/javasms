<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Student Details</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
<h3>STUDENT DETAILS</h3>


  <table id="studTable" class="table table-bordered">
	<thead>
		<tr> 
		   <th> ID </th>
		   <th>Name</th>
		   <th>Subject </th>
		   <th>Address </th>
		   <th>Email </th>
		   <th>Contact no </th>
		   <th>Joined date </th>
		   <th>Course start date </th>
		   <th>Course end date </th>
		   <th>Fee </th>
		   <th></th>
		  </tr>
	</thead>
  
 
  
  <tbody>
      <c:forEach var="row" items="${studentList}">
      <tr>
         <td> ${row.id} </td>
         <td> ${row.firstName }, &nbsp; ${row.lastName } </td>
         <td> ${row.subject } </td>
         <td> ${row.address } </td>
         <td> ${row.email } </td>
         <td> ${row.contactno } </td>
         <td> <fmt:formatDate value="${row.joineddate }" pattern="MM/dd/yyyy"/> </td>
         <td> <fmt:formatDate value="${row.coursestartdate }" pattern="MM/dd/yyyy"/> </td>
         <td> <fmt:formatDate value="${row.courseenddate }" pattern="MM/dd/yyyy"/> </td>
         <td> ${row.fee } </td>
         <td>
         <button class="btn btn-primary" onclick="editStudent(${row.id })"> Edit </button>
         <button class="btn btn-danger" onclick="deleteStud(${row.id })"> Delete </button>
         
         </td>
      </tr>
      
      </c:forEach>
   </tbody>
  </table>
  </div>
 
 <div id="modalHolder">
 </div>
  
 
  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
      
  <script type="text/javascript">
  
 	  
  function editStud(id){
		var editStudUrl="${pageContext.request.contextPath}/studdet/" + id + "/edit";
        $("#studentModal").modal("show");
  }
function editStudent(id){
		var editStudUrl="${pageContext.request.contextPath}/studdet/" + id + "/edit";
		 $.ajax({
		      url: editStudUrl,
		      success: function(data) {
		          console.log(data);
       			$("#modalHolder").html(data);
		          $("#studentModal").modal("show");
		          }
		      });

}
 
  			function deleteStud(id){
  				var r = confirm("Are you Sure to delete this record?");
  				if( r==true){
  					location.href = "${pageContext.request.contextPath}/studdet/" + id + "/delete";
  				}
  			}
  			
  			$(document).ready(function(){
  			$("#studTable").DataTable();
  			});
  </script>
  