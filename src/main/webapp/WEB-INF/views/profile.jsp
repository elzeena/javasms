<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en-US">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <title>Institute Management System</title>
      <link rel="stylesheet" href="${pageContext.request.contextPath }/resources/css/components.css">
      <link rel="stylesheet" href="${pageContext.request.contextPath }/resources/css/responsee.css">
      <link rel="stylesheet" href="${pageContext.request.contextPath }/resources/css/icons.css">
      <link rel="stylesheet" href="${pageContext.request.contextPath }/resources/owl-carousel/owl.carousel.css">
      <link rel="stylesheet" href="${pageContext.request.contextPath }/resources/owl-carousel/owl.theme.css">
      <!-- CUSTOM STYLE -->
      <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
      
      <link rel="stylesheet" href="${pageContext.request.contextPath }/resources/css/template-style.css">
      <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
      <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
      <!-- 
      <script type="text/javascript" src="${pageContext.request.contextPath }/resources/js/jquery-1.8.3.min.js"></script>
        -->
      <script type="text/javascript" src="${pageContext.request.contextPath }/resources/js/jquery-ui.min.js"></script>    
      <script type="text/javascript" src="${pageContext.request.contextPath }/resources/js/template-scripts.js"></script> 
   </head>
   <body class="size-1140">
  	  <!-- PREMIUM FEATURES BUTTON -->
  	  <a target="_blank" class="hide-s" href="../template/onepage-premium-template/" style="position:fixed;top:130px;right:-14px;z-index:10;"><img src="img/premium-features.png" alt=""></a>
      <!-- TOP NAV WITH LOGO -->
      <header>
         <div id="topbar">
            <div class="line">
               <div class="s-12 m-6 l-6">
                  <p>CONTACT US: <strong>9804000000</strong> | <strong>info@instituation.com </strong></p>
               </div>
               <div class="s-12 m-6 l-6">
                  <div class="social right">
                     <a><i class="icon-facebook_circle"></i></a> <a><i class="icon-twitter_circle"></i></a> <a><i class="icon-google_plus_circle"></i></a> <a><i class="icon-instagram_circle"></i></a>
                  </div>
               </div>
            </div>  
         </div> 
         <nav>
            <div class="line">
               <div class="s-12 l-2">
                  <p class="logo"><strong>Insti</strong>tue</p>
               </div>
               <div class="top-nav s-12 l-10">
                  <p class="nav-text">Custom menu text</p>
                  <ul class="right">
                     <li class="active-item"><a href="${pageContext.request.contextPath }/profile">Home</a></li>
                     <li><a href="#features">Features</a></li>
                     <li><a href="#about-us">About Us</a></li>
                     <li><a href="">Institute Info System</a>
                     	<ul class="dropdown-menu" aria-labelledby="navbarDropdown">
						<li><a href="${pageContext.request.contextPath }/stud" class="dropdown-item">Add Student</a></li>
						<li><a href="${pageContext.request.contextPath }/studdet"class="dropdown-item">Student Details</a></li>
							                      
						</ul></li>
                     <li><a href="#services">Services</a></li>
                     <li><a href="#contact">Contact</a></li>
                  </ul>
               </div>
            </div>
         </nav>
      </header>  
      <section>
         <!-- CAROUSEL --> 
         <div id="carousel">
            <div id="owl-demo" class="owl-carousel owl-theme"> 
               <div class="item">
                  <img src="${pageContext.request.contextPath }/resources/img/img1.jpg" alt="">
                  <div class="line"> 
                     <div class="text hide-s">
                        <div class="line"> 
                          <div class="prev-arrow hide-s hide-m">
                             <i class="icon-chevron_left"></i>
                          </div>
                          <div class="next-arrow hide-s hide-m">
                             <i class="icon-chevron_right"></i>
                          </div>
                        </div> 
                        <h2>Institute Management System</h2>
                        <p>My Institute online management software is a fully featured, secure web based institute managment system with great support.</p>
                     </div>
                  </div>
               </div>
               <div class="item">
                  <img src="${pageContext.request.contextPath }/resources/img/img2.jpg" alt="">
                  <div class="line">
                     <div class="text hide-s"> 
                        <div class="line"> 
                          <div class="prev-arrow hide-s hide-m">
                             <i class="icon-chevron_left"></i>
                          </div>
                          <div class="next-arrow hide-s hide-m">
                             <i class="icon-chevron_right"></i>
                          </div>
                        </div> 
                        <h2>Institute Management System</h2>
                        <p>My Institute online management software is a fully featured, secure web based institute managment system with great support.</p>
                     </div>
                  </div>
               </div>
               <div class="item">
                  <img src="${pageContext.request.contextPath }/resources/img/img3.jpg" alt="">
                  <div class="line">
                     <div class="text hide-s">
                        <div class="line"> 
                          <div class="prev-arrow hide-s hide-m">
                             <i class="icon-chevron_left"></i>
                          </div>
                          <div class="next-arrow hide-s hide-m">
                             <i class="icon-chevron_right"></i>
                          </div>
                        </div> 
                        <h2>Institute Management System</h2>
                        <p>My Institute online management software is a fully featured, secure web based institute managment system with great support.</p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         
         <!-- FIRST BLOCK -->
         <div id="first-block">
            <div class="line">
               <h1>welcome to Institute Management System</h1>
               <p>An intutive and powerful interface that requires little training and ensures that work gets done quickly.</p>
               <div class="s-12 m-4 l-2 center"><a class="white-btn" href="#contact">Contact Us</a></div>
            </div>
         </div>
         
         
         <!-- FEATURES -->
         <div id="features">
            <div class="line">
               <div class="margin">
                  <div class="s-12 m-6 l-3 margin-bottom">
                     <i class="icon-tablet icon3x"></i>
                     <h2>fast implementation</h2>
                     <p>painless data migration from platforms</p>
                  </div>
                  <div class="s-12 m-6 l-3 margin-bottom">
                     <i class="icon-refresh"></i>
                     <h2>easy to use</h2>
                     <p>minimal training required, even for less savy users.</p>
                  </div>
                  <div class="s-12 m-6 l-3 margin-bottom">
                     <i class="icon-random"></i>
                     <h2>Flexible</h2>
                     <p>most aspects of my institute are configurable to suit your needs.</p>
                  </div>
                  <div class="s-12 m-6 l-3 margin-bottom">
                     <i class="icon-users"></i>
                     <h2>great support</h2>
                     <p>friendly and professional staff who are ready to help you.</p>
                  </div>
               </div>
            </div>
         </div>
         <!-- ABOUT US -->
         <div id="about-us">
            <div class="s-12 m-12 l-6 media-container">
               <img src="${pageContext.request.contextPath }/resources/img/about.jpg" alt="">
            </div>
            <article class="s-12 m-12 l-6">
               <h2>About</h2>
               <p>It is a complete learning institute that not only provides training on various IT courses but also prepares students to smartly
               handle the real working environment. Our well facilitated infrastructures, well-furnished labs and classrooms,peaceful learning
               environment, updated courses,qualified and experts trainers,sedate location, enough parking, soft skills training etc. make us 
               distinct from numerous other IT training and developing institution.Weare marching forward systematically to compete the global
               market scenario.
               our success is based n innovation, competence and service.International quality in training,technical backup and reliability, 
               innovative and up to-date teaching-learning faculties are our strength that make us a successful and leading IT training Instituation 
               in nepal. we have been incorporating with our institutional extension in USA since the inception of this institution.
               </p>
               <div class="about-us-icons">
                  <i class="icon-paperplane_ico"></i> <i class="icon-trophy"></i> <i class="icon-clock"></i>
               </div>
            </article>
         </div>
         <!-- OUR WORK -->
         
         
         
         
         <!-- SERVICES -->
         <div id="services">
            <div class="line">
               <h2 class="section-title">services</h2>
               <div class="margin">
                  <div class="s-12 m-6 l-4 margin-bottom">
                     <i class="icon-user"></i>
                     <div class="service-text">
                        <h3>Individual</h3>
                        <p> Adressing the needs of trainees, individual training classes are also organized where
                        the trainee can study alone with the course instructor at agreeable training fees. </p>
                     </div>
                  </div>
                  <div class="s-12 m-6 l-4 margin-bottom">
                     <i class="icon-eye"></i>
                     <div class="service-text">
                        <h3>group</h3>
                        <p>we offer regular training sessions with maximum 10 students in a group as per the 
                        interested course making it easy and convenient for the trainees and the trainer to study
                        more effectively.</p>
                     </div>
                  </div>
                  <div class="s-12 m-12 l-4 margin-bottom">
                     <i class="icon-random"></i>
                     <div class="service-text">
                        <h3>corporate</h3>
                        <p>we also provide corporate training facility to the corporate organizations who require 
                        training to be provided in mass for the particular level of employees in any required course.</p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- LATEST NEWS -->
         <div id="latest-news">
            <div class="line">
              <h2 class="section-title">Latest News</h2> 
              <div class="margin">
                <div class="s-12 m-6 l-6">
                  <div class="s-12 l-2">
                    <div class="news-date">
                      <p class="day">28</p><p class="month">AUGUST</p><p class="year">2015</p>
                    </div>
                  </div>
                  <div class="s-12 l-10">
                    <div class="news-text">
                      <h4>First latest News</h4>
                      <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam.</p>
                    </div>
                  </div>   
                </div> 
                <div class="s-12 m-6 l-6">
                  <div class="s-12 l-2">
                    <div class="news-date">
                      <p class="day">12</p><p class="month">JULY</p><p class="year">2015</p>
                    </div>
                  </div>
                  <div class="s-12 l-10">
                    <div class="news-text">
                      <h4>Second latest News</h4>
                      <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam.</p>
                    </div>
                  </div>   
                </div>  
              </div>
            </div>
         </div> 
         <!-- CONTACT -->
         <div id="contact">
            <div class="line">
               <h2 class="section-title">Contact Us</h2>
               <div class="margin">
                  <div class="s-12 m-12 l-3 hide-m hide-s margin-bottom right-align">
                    <img src="${pageContext.request.contextPath }/resources/img/contact.jpg" alt="">
                  </div>
                  <div class="s-12 m-12 l-4 margin-bottom right-align">
                     <h3>Vision Design - graphic zoo</h3>
                     <address>
                        <p><strong>Adress:</strong> tinkune</p>
                        <p><strong>Country:</strong> kathmandu </p>
                        <p><strong>E-mail:</strong> info@instituation.com</p>
                     </address>
                     <br />
                     
                  </div>
                  <div class="s-12 m-12 l-5">
                    <h3> contact form</h3>
                    <form class="customform" action="">
                      <div class="s-12"><input name="" placeholder="Your e-mail" title="Your e-mail" type="text" /></div>
                      <div class="s-12"><input name="" placeholder="Your name" title="Your name" type="text" /></div>
                      <div class="s-12"><textarea placeholder="Your message" name="" rows="5"></textarea></div>
                      <div class="s-12 m-12 l-4"><button class="color-btn" type="submit">Submit Button</button></div>
                    </form>
                  </div>                
               </div>
            </div>
         </div>
         <!-- MAP -->
         <div id="map-block">  	  
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1247814.3661917313!2d16.569872019090596!3d48.23131953825178!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x476c8cbf758ecb9f%3A0xddeb1d26bce5eccf!2sGallayova+2150%2F19%2C+841+02+D%C3%BAbravka!5e0!3m2!1ssk!2ssk!4v1440344568394" width="100%" height="450" frameborder="0" style="border:0"></iframe>
         </div>
      </section>
      <!-- FOOTER -->
      <footer>
         
      </footer>
      <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath }/resources/js/responsee.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath }/resources/owl-carousel/owl.carousel.js"></script>
      <script type="text/javascript">
         jQuery(document).ready(function($) {
            var theme_slider = $("#owl-demo");
            var owl = $('#owl-demo');
            owl.owlCarousel({
              nav: false,
              dots: true,
              items: 1,
              loop: true,
              autoplay: true,
              autoplayTimeout: 6000
            });
            var owl = $('#owl-demo2');
            owl.owlCarousel({
              nav: true,
              dots: false,
              items: 1,
              loop: true,
              navText: ["&#xf007","&#xf006"],
              autoplay: true,
              autoplayTimeout: 4000
            });
        
            // Custom Navigation Events
            $(".next-arrow").click(function() {
                theme_slider.trigger('next.owl');
            })
            $(".prev-arrow").click(function() {
                theme_slider.trigger('prev.owl');
            })     
        }); 
      </script>
   </body>
</html>