<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title>Login form</title>
  
  
  
      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">

  <style type="text/css">
.error{
	color:red
}
</style>
</head>

<body>

  <body>
<div class="container">
	<section id="content">
	<form action="profile" method="post" model="user" accept-charset="utf-8">
		
			<h1>Login Form</h1>
			<div>
				<input type="text" placeholder="Username" name="username" id="username" />
				<h3 class="error"> ${loginError} </h3>
			</div>
			<div>
				<input type="password" placeholder="Password" name="password" id="password" />
			</div>
			<div>
				<input type="submit" value="Log in" />
				
			</div>
		</form><!-- form -->
		
	</section><!-- content -->
</div><!-- container -->
</body>
  
  

    <script  src="${pageContext.request.contextPath}/resources/js/index.js"></script>




</body>

</html>
