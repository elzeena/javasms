<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

        <title>Institute Management System</title>

        <!--    Google Fonts-->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>

        <!--Fontawesom-->
        <link rel="stylesheet" href="${pageContext.request.contextPath }/resources/css/font-awesome.min.css">

        <!--Animated CSS-->
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resources/css/animate.min.css">

        <!-- Bootstrap -->
        <link href="${pageContext.request.contextPath }/resources/css/bootstrap.min.css" rel="stylesheet">
        <!--Bootstrap Carousel-->
        <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath }/resources/css/carousel.css" />

        <link rel="stylesheet" href="${pageContext.request.contextPath }/resources/css/isotope/style.css">
        <!--Themefy Icons-->
        <link href="${pageContext.request.contextPath }/resources/vendor/themefyicon/themify-icons.css" rel="stylesheet">
        <!--Main Stylesheet-->
        <link href="${pageContext.request.contextPath }/resources/css/style.css" rel="stylesheet">
        <!--Custom Stylesheet-->
        <link href="${pageContext.request.contextPath }/resources/css/main.css" rel="stylesheet">
        <!--Responsive Framework-->
        <link href="${pageContext.request.contextPath }/resources/css/responsive.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        </head>




<!--Start of welcome section-->
        <section id="welcome">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="wel_header">
                            <h2>welcome to Institute Management System</h2>
                            <p>An intutive and powerful interface that requires little training and ensures that work gets done quickly. </p>
                        </div>
                    </div>
                </div>
                <!--End of row-->
                <div class="row">
                    <div class="col-md-3">
                        <div class="item">
                            <div class="single_item">
                                <div class="item_list">
                                    <div class="welcome_icon">
                                        <i class="fa ti-stats-up"></i>
                                    </div>
                                    <h4>fast implementation</h4>
                                    <p>painless data migration from platforms</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End of col-md-3-->
                    <div class="col-md-3">
                        <div class="item">
                            <div class="single_item">
                                <div class="item_list">
                                    <div class="welcome_icon">
                                        <i class="fa fa-refresh"></i>
                                    </div>
                                    <h4>easy to use </h4>
                                    <p>minimal training required, even for less savy users.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End of col-md-3-->
                    <div class="col-md-3">
                        <div class="item">
                            <div class="single_item">
                                <div class="item_list">
                                    <div class="welcome_icon">
                                        <i class="fa fa-tint"></i>
                                    </div>
                                    <h4>Flexible </h4>
                                    <p>most aspects of my institute are configurable to suit your needs.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End of col-md-3-->
                    <div class="col-md-3">
                        <div class="item">
                            <div class="single_item">
                                <div class="item_list">
                                    <div class="welcome_icon">
                                      <i class="fa fa-hands-helping"></i>
                                    </div>
                                    <h4>great support</h4>
                                    <p>friendly and professional staff who are ready to help you. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End of col-md-3-->
                </div>
                <!--End of row-->
            </div>
            <!--End of container-->
        </section>
        <!--end of welcome section-->


