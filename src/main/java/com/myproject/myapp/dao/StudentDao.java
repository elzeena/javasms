package com.myproject.myapp.dao;

import java.util.List;

import com.myproject.myapp.model.Student;

public interface StudentDao {

	void insertUpdate(Student student);
	List<Student> getAll();
	Student get(Long id);
	void delete(Long id);
}
