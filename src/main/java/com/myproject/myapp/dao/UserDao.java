package com.myproject.myapp.dao;

import com.myproject.myapp.model.User;

public interface UserDao {
	boolean validateUser(User user);
}
