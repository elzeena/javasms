package com.myproject.myapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.myproject.myapp.dao.StudentDao;
import com.myproject.myapp.model.Student;


@RestController
@RequestMapping("/api/rest")
public class StudentRestController {

	@Autowired
	private StudentDao studentDao;
	
	@RequestMapping(value="/students", method = RequestMethod.GET)
	public ResponseEntity<List<Student>> studGetAll() {
		
		List<Student> studList = studentDao.getAll();
		
		ResponseEntity<List<Student>> resp = new ResponseEntity<>(studList,HttpStatus.OK);
		return resp;
	}
}
