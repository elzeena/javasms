package com.myproject.myapp.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.myproject.myapp.service.FileStorageService;


@Controller
public class FileUploadDownloadController {
	
	@Autowired
	private FileStorageService fileStorageService;
	@RequestMapping(value="/upload", method = RequestMethod.GET)
	public String uploadGET() {
		return "fileUpload";
	}

	@RequestMapping(value="/upload", method = RequestMethod.POST)
	public String uploadPOST(@RequestParam("file") MultipartFile[] files, Model model) throws UnsupportedEncodingException {
		
		List<String> fileNames = new ArrayList<>();
		for(MultipartFile file : files) {
			if(!file.isEmpty()) {
				//save in drive
				fileStorageService.saveFile(file);
				
				fileNames.add(URLEncoder.encode(file.getOriginalFilename(),"UTF-8"));
				
				
			}
		}
		
		model.addAttribute("successMsg","File Upload success, No of files:" + files.length);
		model.addAttribute("fileNames", fileNames);
		
		return "fileUpload";
	}
	
	@RequestMapping(value = "/download" , method = RequestMethod.GET)
	public void download(@RequestParam("file") String fileName, HttpServletResponse response)  throws IOException {
	
	fileName = URLDecoder.decode(fileName, "UTF-8");
	//flower.jsp
	File imagePath = new File(FileStorageService.FILE_PATH + fileName);
	if (imagePath.exists()) {
		
		String ext = FilenameUtils.getExtension(fileName);
		//setHeader
		if(ext.equals("png") || ext.equals("jpg") || ext.equals("jpeg") || ext.equals("gif")) {
			response.setContentType("image/" +ext);
		}else if (ext.equals("pdf")) {
			response.setContentType("application/" + ext);
		}
		response.setHeader("Content-Disposition", "attachment;filename=" + fileName);

		// Copy file stream and write in response writer or outputstream
		FileCopyUtils.copy(new FileInputStream(imagePath), response.getOutputStream());
		
		//OR
		/*
			PrintWriter out = response.getWriter();
			FileInputStream fin = new FileInputStream(imagePath);

			int i = 0;
			while ((i = fin.read()) != -1) {
				out.write(i);
			}
			fin.close();
			out.close();
		*/

	}

}



}

		

		


