package com.myproject.myapp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.web.multipart.MultipartFile;

import lombok.Data;

@Data
@Entity
@Table(name = "student")
public class Student {

	@Id
	@GeneratedValue
	private Long id;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "subject")
	private String subject;
	
	@Column(name = "address")
	private String address;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "contact_no")
	private String contactno;
	
	@Column(name = "joined_date")
	private Date joineddate;
	
	@Column(name = "course_start_date")
	private Date coursestartdate;
	
	@Column(name = "course_end_date")
	private Date courseenddate;

	@Column(name = "image_name")
	private String imageName;

	@Column(name = "fee")
	private Double fee;
	
	

	@Transient
	private MultipartFile file;

}
